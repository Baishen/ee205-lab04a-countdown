///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author TODO_name <TODO_eMail>
// @date   TODO_dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
int main(int argc, char** argv) {
 time_t rawtime;
 struct tm * timeinfo;
 int year, day, hour, minute, second;
 
  
  time (&rawtime);
  timeinfo = localtime (&rawtime);
     int i = 1;
     while(1) {
        
        printf("i:%d\n", i);
        printf ("Reference time: %s", asctime(timeinfo));
        printf("Years: %d\n", (-114)+timeinfo->tm_year );
        printf("Days: %d\n",(-21)+timeinfo->tm_yday);
        printf("Hours: %d\n",(-4)+timeinfo->tm_hour);
        printf("Minutes: %d\n",(-26)+timeinfo->tm_min);
        printf("Seconds : %d\n",(-7)+timeinfo->tm_sec);
        sleep(1);
     }
  
  return 0;
}
